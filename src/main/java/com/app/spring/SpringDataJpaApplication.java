package com.app.spring;

import com.app.spring.model.Customers;
import com.app.spring.repo.CustomersRepository;
import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringDataJpaApplication {

	public static void main(String[] args) {
            SpringApplication.run(SpringDataJpaApplication.class, args);
	}
        
        
        public CommandLineRunner testCustomerRepository(CustomersRepository repo){
            return a -> {
                List<Customers> list  = repo.findAll();
                System.out.format("%-15s %-40s %-30s %-20s \n", "Customer ID", "Company Name",
                                "Contact Name", "City");
                    System.out.println("--------------------------------------------------------"
                                + "---------------------------------------------");
                list.forEach(c -> {
                    System.out.format("%-15s %-40s %-30s %-20s \n",
                                c.getCustomerID(), c.getCompanyName(),
                                c.getContactName(), c.getCity());
                });
            };
        }

        @Bean
        public CommandLineRunner testInsertCustomer(CustomersRepository repo){
            return a -> {
                try {
                    Customers cus = new Customers();
                    cus.setCustomerID("AABAB");
                    cus.setCompanyName("PT. Selalu Untung");
                    cus.setContactName("Dicky");
                    cus.setContactTitle("Owner");
                    cus.setAddress("Jl. Letjen");
                    cus.setRegion("Bekasi");
                    cus.setPostalCode("17444");
                    cus.setCountry("Indonesia");
                    cus.setPhone("082110049632");
                    cus.setFax("021-4979872");
                
                    repo.save(cus);
                    System.out.println("Save Customers sukses!");
                } catch (Exception e) {
                    System.out.println("Save Customers gagal!");
                    System.out.println("Error : " + e.getMessage());
                }
            };
        }
        
        
        public CommandLineRunner testUpdateCustomer(CustomersRepository repo){
            return a -> {
              Customers cus = repo.findById("AABAB").orElse(null);
              cus.setContactName("Adhi");
              repo.save(cus);
              System.out.println("Customer AABAB ditemukan" + cus.getCompanyName());
            };
        }
        
      
        public CommandLineRunner testDeleteCustomer(CustomersRepository repo){
            return a -> {
              Customers cus = repo.findById("AABAB").orElse(null);
              repo.delete(cus);
              System.out.println("Customer AABAB sudah dihapus");
            };
        }
        
}
